@echo off
@echo Creating angular structure
@echo ---------------------------
mkdir .\pgdb
@echo /pgdb

mkdir .\pgdb\app
@echo ---/app

mkdir .\pgdb\app\components
@echo ------/components

mkdir .\pgdb\app\shared
@echo ------/shared

mkdir .\pgdb\assets
@echo ---/assets

mkdir .\pgdb\assets\js
@echo ------/js

mkdir .\pgdb\assets\css
@echo ------/css

mkdir .\pgdb\assets\libs
@echo ------/libs

mkdir .\pgdb\assets\libs\2.1.4
@echo ---------/2.1.4

mkdir .\pgdb\assets\img
@echo ------/img

@echo Structure created.

PAUSE