(function(t) {
    try {
        t = angular.module("field-dropdown-popup.templates");
    }
    catch (n) {
        t = angular.module("field-dropdown-popup.templates", []);
    }
    t.run(["$templateCache",
        function(t) {
            t.put("dropdown-directive.html", '<div><input type="button" class="button ionicFieldButton" ng-model="choices.chosen" value="{{choices.choice.text}}" /></div>')
        }
    ])
})();
(function(t) {
    try {
        t = angular.module("field-dropdown-popup.templates");
    }
    catch (n) {
        t = angular.module("field-dropdown-popup.templates", []);
    }
    t.run(["$templateCache",
        function(t) {
            t.put("popup-directive.html", '<ion-radio ng-repeat="item in choices" ng-click="closePopup(mypopUp)" ng-model="choices.choice" ng-value="{{item}}">{{item.text | undefinedFilter }}</ion-radio>')
        }
    ])
})();
(function() {
    "use strict";
    angular.module("field-dropdown-popup", ['ionic', 'field-dropdown-popup.templates']);
})();
(function() {
    "use strict";
    angular.module("field-dropdown-popup").directive('fieldDropdownPopup', fieldDropdownPopup);
    fieldDropdownPopup.$inject = ['$ionicPopup'];

    function fillObjects (scope) {
        scope.choices = scope.inputObj.params || [];
        scope.closeLabel = scope.inputObj.closeLabel || "Cancel";
        scope.selectLabel = scope.inputObj.selectLabel || "Select";
        scope.closeButtonType = scope.inputObj.closeButtonType || "button-stable";
        scope.selectButtonType = scope.inputObj.selectButtonType || "button-positive";
    }

    function fieldDropdownPopup($ionicPopup) {
        return {
            restrict: 'AE',
            replace: true,
            templateUrl: 'dropdown-directive.html',
            scope: {
                inputObj: "=inputObj"
            },
            link: function(scope, element, attrs) {
                fillObjects(scope);

                scope.$watch("inputObj.params", function(n, o, s) {
                    if(angular.isDefined(n)) {
                        scope.choices = n;
                    }
                });

                scope.closePopup = function() {
                    scope.myPopup.close();
                    scope.inputObj.callback(scope.choices.choice);
                };

                element.on('click', function() {

                    var pOptions = {
                        templateUrl: 'popup-directive.html',
                        scope: scope
                    }

                    scope.myPopup = $ionicPopup.show(pOptions);
                });
            }
        }
    }
})();